//
//  UMVMainViewController.m
//  umovThreather
//
//  Created by Eder Martins on 9/22/16.
//  Copyright © 2016 edermartins. All rights reserved.
//

#import "UMVMainViewController.h"

@implementation UMVMainViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.ticket = [[UMVTicket alloc] init];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)segmentedControlTypeValueChanged:(id)sender {
    self.ticket.type = self.segmentedControlType.selectedSegmentIndex;
    [self calculate];
}

- (IBAction)segmentedControleWeekValueChanged:(id)sender {
    self.ticket.weekDay = self.segmentedControlWeekDay.selectedSegmentIndex;
    [self calculate];
}

-(void) calculate{
    [self verifyBaseTicketValue];
    [self verifyDiscount];
}

-(void) verifyBaseTicketValue{
    switch (self.ticket.type){
        case 0:
            //Crianças: R$ 5.50
            self.ticket.baseValue = 5.5;
            break;
        case 1:
            //Estudantes: R$ 8.00
            self.ticket.baseValue = 8.0;
            break;
        case 2:
            //Idosos: R$ 6.00
            self.ticket.baseValue = 6.0;
            break;
    }
}


-(void)verifyDiscount{
    self.ticket.discountPercent = 0.0;
    self.ticket.discountValue = 0.0;
    switch (self.ticket.weekDay) {
        // Segunda feira = 10% para todos.
        case 0:
            self.ticket.discountPercent = 0.1;
            break;
            
        // Terca - 15% idosos e criancas, 5% estudantes.
        case 1:
            if(self.ticket.type == 0 || self.ticket.type == 2) self.ticket.discountPercent = 0.15;
            if(self.ticket.type == 1) self.ticket.discountPercent = 0.05;
            break;
            
            // Quarta - 40% idosos, 30% criancas, 50% estudantes
        case 2:
            if(self.ticket.type == 0) self.ticket.discountPercent = 0.3;
            if(self.ticket.type == 1) self.ticket.discountPercent = 0.5;
            if(self.ticket.type == 2) self.ticket.discountPercent = 0.4;
            break;
            
            // Quinta - 30% idosos e estudantes
        case 3:
            if(self.ticket.type == 1 || self.ticket.type == 2) self.ticket.discountPercent = 0.3;
            break;
            
            // Sexta - 11% criancas
        case 4:
            if(self.ticket.type == 0) self.ticket.discountPercent = 0.11;
            break;
            
            //Domingo/sabado/feriados - 5% idosos
        default:
            if(self.ticket.type == 2) self.ticket.discountPercent = 0.05;
            break;
    }
    
    self.ticket.discountValue = self.ticket.baseValue * self.ticket.discountPercent;
    self.ticket.finalValue = self.ticket.baseValue - self.ticket.discountValue;
    [self updateUI];
    
}

-(void)updateUI{
    self.lblDiscount.text = [NSString stringWithFormat:@"Desconto: %.02f",self.ticket.discountValue];
    self.lblBaseValue.text = [NSString stringWithFormat:@"Valor base: %.02f",self.ticket.baseValue];
    self.lblFinalValue.text = [NSString stringWithFormat:@"Valor final: %.02f",self.ticket.finalValue];
}
@end
