//
//  AppDelegate.h
//  umovThreather
//
//  Created by Eder Martins on 9/22/16.
//  Copyright © 2016 edermartins. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

