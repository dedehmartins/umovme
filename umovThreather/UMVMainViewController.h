//
//  UMVMainViewController.h
//  umovThreather
//
//  Created by Eder Martins on 9/22/16.
//  Copyright © 2016 edermartins. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UMVTicket.h"

@interface UMVMainViewController : UIViewController
@property(readwrite) UMVTicket * ticket;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControlWeekDay;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControlType;

@property (weak, nonatomic) IBOutlet UILabel *lblBaseValue;

@property (weak, nonatomic) IBOutlet UILabel *lblDiscount;

@property (weak, nonatomic) IBOutlet UILabel *lblFinalValue;
-(void) calculate;

@end
