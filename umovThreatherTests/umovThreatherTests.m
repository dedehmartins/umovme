//
//  umovThreatherTests.m
//  umovThreatherTests
//
//  Created by Eder Martins on 9/22/16.
//  Copyright © 2016 edermartins. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "UMVMainViewController.h"

@interface umovThreatherTests : XCTestCase

@end

@implementation umovThreatherTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

-(void)testSegunda{
    UMVMainViewController * viewController = [[UMVMainViewController alloc]init];
    [viewController viewDidLoad];
    viewController.ticket.weekDay = 0; // segunda
    
    viewController.ticket.type = 0; // Crianca
    [viewController calculate];
    XCTAssertEqualWithAccuracy(viewController.ticket.finalValue, 4.95f, 0.001, @"");
}

-(void)testTerca{
    UMVMainViewController * viewController = [[UMVMainViewController alloc]init];
    [viewController viewDidLoad];
    
    viewController.ticket.weekDay = 1;
    viewController.ticket.type = 0; // Crianca
    [viewController calculate];
    XCTAssertEqualWithAccuracy(viewController.ticket.finalValue, 5.5*(1-0.15), 0.001, @"");
}

-(void)testQuarta{
    UMVMainViewController * viewController = [[UMVMainViewController alloc]init];
    [viewController viewDidLoad];
    
    viewController.ticket.weekDay = 2;
    viewController.ticket.type = 0; // Crianca
    [viewController calculate];
    XCTAssertEqualWithAccuracy(viewController.ticket.finalValue, 5.5*(1-0.3), 0.001, @"");
    viewController.ticket.type = 1; // estudante
    [viewController calculate];
    XCTAssertEqualWithAccuracy(viewController.ticket.finalValue, 8.0*(1-0.5), 0.001, @"");
    
    viewController.ticket.type = 2; // idoso
    [viewController calculate];
    XCTAssertEqualWithAccuracy(viewController.ticket.finalValue, 6.0*(1-0.4), 0.001, @"");
}

-(void)testQuinta{
    UMVMainViewController * viewController = [[UMVMainViewController alloc]init];
    [viewController viewDidLoad];
    
    viewController.ticket.weekDay = 3;
    viewController.ticket.type = 0; // Crianca
    [viewController calculate];
    XCTAssertEqualWithAccuracy(viewController.ticket.finalValue, 5.5, 0.001, @"");
    viewController.ticket.type = 1; // estudante
    [viewController calculate];
    XCTAssertEqualWithAccuracy(viewController.ticket.finalValue, 8.0*(1-0.3), 0.001, @"");
    
    viewController.ticket.type = 2; // idoso
    [viewController calculate];
    XCTAssertEqualWithAccuracy(viewController.ticket.finalValue, 6.0*(1-0.3), 0.001, @"");
}

-(void)testSexta{
    UMVMainViewController * viewController = [[UMVMainViewController alloc]init];
    [viewController viewDidLoad];
    
    viewController.ticket.weekDay = 4;
    viewController.ticket.type = 0; // Crianca
    [viewController calculate];
    XCTAssertEqualWithAccuracy(viewController.ticket.finalValue, 5.5*(1-0.11), 0.001, @"");
    viewController.ticket.type = 1; // estudante
    [viewController calculate];
    XCTAssertEqualWithAccuracy(viewController.ticket.finalValue, 8.0, 0.001, @"");
    
    viewController.ticket.type = 2; // idoso
    [viewController calculate];
    XCTAssertEqualWithAccuracy(viewController.ticket.finalValue, 6.0, 0.001, @"");
}

-(void)testDomingo{
    UMVMainViewController * viewController = [[UMVMainViewController alloc]init];
    [viewController viewDidLoad];
    
    viewController.ticket.weekDay = 5;
    viewController.ticket.type = 0; // Crianca
    [viewController calculate];
    XCTAssertEqualWithAccuracy(viewController.ticket.finalValue, 5.5, 0.001, @"");
    viewController.ticket.type = 1; // estudante
    [viewController calculate];
    XCTAssertEqualWithAccuracy(viewController.ticket.finalValue, 8.0, 0.001, @"");
    
    viewController.ticket.type = 2; // idoso
    [viewController calculate];
    XCTAssertEqualWithAccuracy(viewController.ticket.finalValue, 6.0*(1-0.05), 0.001, @"");
}

@end
