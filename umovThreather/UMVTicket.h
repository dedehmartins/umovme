//
//  UMVTicket.h
//  umovThreather
//
//  Created by Eder Martins on 9/22/16.
//  Copyright © 2016 edermartins. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UMVTicket : NSObject
    @property(readwrite) int type;
    @property(readwrite) int weekDay;
@property(readwrite) float baseValue;
@property(readwrite) float discountPercent;
@property(readwrite) float discountValue;
    @property(readwrite) float finalValue;
@end
